<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Logout</title>
	<link rel="stylesheet" href="bootstrap.css">
</head>
<style>
.table
{
	margin-top: 20px;
}
</style>
<body>
	<?php
	session_start();
	include_once 'config.php';
	date_default_timezone_set("Asia/Kolkata"); 
if(!isset($_SESSION['email']))
{
  header("location:index.php");
}
?>
	<div class="container-fluid" <?php  echo $_SESSION['email']; ?> >
		<table class="table table-hover" style="border-style: solid;" border="" width="85%">
			<tr align="center">
				<td>Id</td>
				<td>First Name</td>
				<td>Last Name</td>
				<td>Email</td>
				<td>Password</td>
				<td>Gender</td>
				<td>Mobile No.</td>
				<td>Country</td>
				<td>Hobbies</td>
				<td>Images</td>
			</tr>
			<tr align="center">
				<td><?php echo $_SESSION['user_id'];?></td>
				<td><?php echo $_SESSION['fname'];?></td>
				<td><?php echo $_SESSION['lname'];?></td>
				<td><?php echo $_SESSION['email'];?></td>
				<td><?php echo $_SESSION['password'];?></td>
				<td><?php echo $_SESSION['Gender'];?></td>
				<td><?php echo $_SESSION['number'];?></td>
				<td><?php echo $_SESSION['country'];?></td>
				<td><?php echo $_SESSION['hobbies'];?></td>
				<td><img src="image/<?php echo $_SESSION['image']; ?>" height="50" width="50" /></td>
			</tr>
		</table>
	</div>
	<br/><br/>
	<div class="container">
	    <div class="col-md-offset-4 col-md-5">
			<a href="display.php" class="btn btn-primary btn-lg">Edit Data</a>&#160;&#160;&#160;&#160;
	        <a href="index.php" class="btn btn-primary btn-lg" <?php session_destroy(); ?>>Logout</a>
		</div>
	</div>
</body>
</html>