<?php
include ("config.php");
error_reporting(0);

$id = $_GET['eid'];

$query = "SELECT * FROM `verify` WHERE `id` = '$id' ";
$run_query = mysqli_query($conn, $query);
$row_query = mysqli_num_rows($run_query);
if ($row_query == 1)
{
	$fetch_query = mysqli_fetch_assoc($run_query);
	$hobby = explode(', ',$fetch_query['hobbies']);

	$uid = $fetch_query['id'];
	$fname = $fetch_query['fname'];
	$lname = $fetch_query['lname'];
	$email = $fetch_query['email'];
	$pass = $fetch_query['password'];
	$gender = $fetch_query['Gender'];
	$num = $fetch_query['number'];
	$country = $fetch_query['country'];
	$hobby = $fetch_query['hobbies'];
	$image = $fetch_query['image'];
}


if (isset($_POST['update']))
{
	date_default_timezone_set("Asia/kolkata");
	$date = date("d-m-y");
	$time = date("h:i:sa");

	$fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$email = $_POST['email'];
	$pass = $_POST['password'];
	$gender = $_POST['Gender'];
	$num = $_POST['number'];
	$country = $_POST['country'];
	$hobby = implode(', ',$_POST['hobbies']);
	$image = $_FILES['image']['name'];
	$image_tmp = $_FILES['image']['tmp_name'];
	$image_type = $_FILES['image']['type'];
	$path = "image/$image_name";


	move_uploaded_file($image_tmp,$path);			

	$query = "SELECT fname FROM `verify` WHERE fname = '$fname'";
	$result = mysqli_query($conn, $query);
	if(mysqli_num_rows($result)>0)

	{
		$query = "UPDATE verify SET `lname` = '$lname', `email` = '$email', `password` = '$pass', `Gender` = '$gender', `number` = '$num', `country` = '$country', `hobbies` = '$hobby', `image` = '$image', `date` = '$date', `time` = '$time' WHERE `fname` = '$fname'";
		$result = mysqli_query($conn, $query);
		if(isset($result))
		{
			echo "<script type='text/javascript'>
			alert('Data Updated Successfully');
			window.location = 'display.php'; 
			</script> ";
		}

	}
	else
	{
		echo "User Not Found";
	}

	if(empty($fname))
	{
		$fnerror  = "* Required !";
	}
	else 
	{
		if (!preg_match("/^[a-zA-Z ]*$/",$fname)) 
		{
			$fnerror = "Only letters and white space allowed";
		}
	}
	if(empty($lname))
	{
		$lnerror="* Required !";
	}
	else 
	{
		if (!preg_match("/^[a-zA-Z ]*$/",$lname)) 
		{
			$lnerror = "Only letters and white space allowed";
		}
	}
	if(empty($email))
	{
		$emerror="* Required !";
	}
	if(empty($pass))
	{
		$perror="* Required !";
	}
	if(empty($gender))
	{
		$gnerror="* Required !";
	}
	if(empty($num))
	{
		$nerror="* Required !";
	}
	if(empty($country))
	{
		$cerror="* Required !";
	}
	if(empty($hobby))
	{
		$herror="* Required !";
	}
	if(empty($image))
	{
		$ierror="* Required !";
	}

}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Update</title>
	<link rel="stylesheet" href="bootstrap.css">
</head>
<style>
.container
{
	margin-top: 40px;
}
.set
{
    margin-top: 2px;
  	margin-left: 190px;
  	font-size: 20px;
}
</style>
<body>
	<div class="container">
		<form method="post" class="form-horizontal" enctype="multipart/form-data" style="background-color: lightgray">
			<div class="panel-heading">
				<h3 class="panel-title"><center>REGISTRATION FORM</center></h3><br>
			</div>
			<label class="control-label col-sm-2" for="fname">First name:-</label>
			<div class="col-sm-10"><input type="text" class="form-control" placeholder="First Name" name="fname" value="<?php echo $fetch_query['fname']?>">
				<span style="color: red"><?php echo $fnerror; ?></span>
			</div>
			<br><br>

			<label class="control-label col-sm-2" for="lname">Last name:-</label>
			<div class="col-sm-10"><input type="text" class="form-control" placeholder="Last Name" name="lname" value="<?php echo $fetch_query['lname']?>">
				<span style="color: red"><?php echo $lnerror; ?></span>
			</div>
			<br><br>

			<label class="control-label col-sm-2" for="lname">Email:-</label>
			<div class="col-sm-10"><input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $fetch_query['email']?>" pattern="(?=.*\d)(?=.*[a-z0-9]+\@([a-z])+\.([a-z]))+$" title="Email ex:- Nitesh25@gmail.com">
				<span style="color: red"><?php echo $emerror; ?></span>
			</div>
			<br><br>

			<label class="control-label col-sm-2" for="password">Password:-</label>
			<div class="col-sm-10"><input type="pass" class="form-control" placeholder="Password" name="password" value="<?php echo $fetch_query['password']?>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}" title="Password Must Be 8 Charectors Example@123">
				<span style="color: red"><?php echo $perror; ?></span>
			</div>
			<br><br>

			<label class="control-label col-sm-2" for="Gender">Gender:-</label>
			<div class="col-sm-3"><label><input type="radio" name="Gender" value="Male<?php if($fetch_query['Gender'] == Male)?>">Male</label></div>
			<div class="col-sm-3"><label><input type="radio" name="Gender" value="Female<?php if($fetch_query['Gender'] == Female)?>">Female</label></div>
			<div class="col-sm-3"><label><input type="radio" name="Gender" value="Other<?php if($fetch_query['Gender'] == Other)?>">Other</label>
				<span style="color: red"><?php echo $gnerror; ?></span>
			</div>
			<br><br>

			<label class="control-label col-sm-2" for="number">Mobile No:-</label>
			<div class="col-sm-10"><input type="num" class="form-control" placeholder="Number" name="number" value="<?php echo $fetch_query['number']; ?>" pattern="(?=,*\d)(?=,*[0-9]).{10,}" title="Number Must Be 10 Digits">
			<span style="color: red"><?php echo $nerror; ?></span>
		    </div>
			<br><br>

			<label class="control-label col-sm-2" for="country">Country:-</label>
			<div class="col-sm-10"><input list="country" class="form-control" placeholder="country" name="country" value="<?php echo $fetch_query['country']; ?>">
			<datalist id="country">
				<option value="">-- Select --</option>
				<option value="India" <?php if($fetch_query['country'] == India) {echo "selected";} ?>></option>
				<option value="London" <?php if($fetch_query['country'] == London) {echo "selected";} ?>></option>
				<option value="Argentina" <?php if($fetch_query['country'] == Argentina) {echo "selected";} ?>></option>
				<option value="South Africa "<?php if($fetch_query['country'] == Africa) {echo "selected";} ?>></option>
				<option value="Canada"<?php if($fetch_query['country'] == Canada) {echo "selected";} ?>></option>
			</datalist>
			<span style="color: red"><?php echo $cerror; ?></span>
		</div>
			<br><br>

			<label class="control-label col-sm-2" for="hobbies">Hobbies:-</label>
			<div class="col-sm-2"><label><input type="checkbox" name="hobbies[]" value="Sports" <?php if(in_array("Sports", $hobby)) {echo "checked";} ?>>&nbsp;Sports</label></div>
			<div class="col-sm-2"><label><input type="checkbox" name="hobbies[]" value="Travalling" <?php if(in_array("Travalling", $hobby)) {echo "checked";} ?>>&nbsp;Travalling</label></div>
			<div class="col-sm-2"><label><input type="checkbox" name="hobbies[]" value="Dancing" <?php if(in_array("Dancing", $hobby)) {echo "checked";} ?>>&nbsp;Dancing</label></div>
			<div class="col-sm-2"><label><input type="checkbox" name="hobbies[]" value="Singing" <?php if(in_array("Singing", $hobby)) {echo "checked";} ?>>&nbsp;Singing</label></div>
			<span style="color: red"><?php echo $herror; ?></span>
			<br><br>

			<label class="control-label col-sm-2" for="image">File Upload:-</label>
			<div class="col-sm-10"><input type="file" name="image" value="<?php echo $fetch_query['image'] ?>">
			<p><?php $fetch_query['image'];?></p>
			<span style="color: red"><?php echo $ierror; ?></span>
		</div>
			<br><br>
			<div class="form-group">
				<div class="col-md-offset-4 col-md-4">
			<input type="submit" class="btn btn-md btn-success btn-block" value="update" name="update">
		</div>
	</div>
		</form> 
	</div>
	
	<div class="set"><a href="display.php">Data Updated Please View</a></div>
</body>
</html>