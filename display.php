<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>View Data</title>
        <link rel="stylesheet" href="bootstrap.css">
    </head>
    <style>
        .set{
        margin-top: -25px;
        margin-left: 15px;
        font-size: 20px;
        }
        .table
        {
        margin-top: 20px;
        }
    </style>
    <body>
    </body>
</html>
<?php
    include("config.php");
    
    function showData()
    {
    
    	global $conn;
    	$query = "SELECT * FROM `verify` ";
    	$set_query = mysqli_query($conn,$query);
    	if($set_query == TRUE)
    
    	{
    
    		?>
<div class="container-fluid">
    <table class="table table-hover">
        <tr align="center">
            <td><b>Id</b></td>
            <td><b>First Name</b></td>
            <td><b>Last Name</b></td>
            <td><b>Email</b></td>
            <td><b>Password</b></td>
            <td><b>Gender</b></td>
            <td><b>Mobile No.</b></td>
            <td><b>Country</b></td>
            <td><b>Hobbies</b></td>
            <td><b>Images</b></td>
            <td><b>Date</b></td>
            <td><b>Time</b></td>
            <td colspan="4">Operation</td>
        </tr>
        <?php
            while($data = mysqli_fetch_assoc($set_query))
                          
            {
            	?>
        <tr align="center">
            <td><?php echo $data['id']?></td>
            <td><?php echo $data['fname']?></td>
            <td><?php echo $data['lname']?></td>
            <td><?php echo $data['email']?></td>
            <td><?php echo $data['password']?></td>
            <td><?php echo $data['Gender']?></td>
            <td><?php echo $data['number']?></td>
            <td><?php echo $data['country']?></td>
            <td><?php echo $data['hobbies']?></td>
            <td><img src="image/<?php echo $data['image']; ?>" height="50" width="50" /></td>
            <td><?php echo $data['date']?></td>
            <td><?php echo $data['time']?></td>
            <td><a href="edit.php?eid=<?php echo $data['id'];?>">Edit</a></td>
            <td><a href="delete.php?eid=<?php echo $data['id'];?>">Delete</a></td>
        </tr>
        <?php
            }
            ?>
    </table>
</div>
<?php
    }
    else
    {
    	echo "Error !";
    }
    }
    ?>
<?php showData(); ?>
<div class="set"><a href="insert.php">Reinsert Data In Table</a></div>