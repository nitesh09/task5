<?php
    // defined('BASEPATH') OR exit('No direct script access allowed'); //This sentax is use for not directly path on server and this vary use full; 
    include("config.php");
    error_reporting(0);
    
    if (isset($_POST['submit']))
    {
    	date_default_timezone_set("Asia/kolkata");
    	$date = date("d-m-y");
    	$time = date("h:i:sa");
    
    	$fname = $_POST['fname'];
    	$lname = $_POST['lname'];
    	$email = $_POST['email'];
    	$pass = $_POST['password'];
    	$gender = $_POST['Gender'];
    	$num = $_POST['number'];
    	$country = $_POST['country'];
    	$hobby =implode(', ',$_POST['hobbies']);
    	$image = $_FILES['image']['name'];
    	$image_tmp = $_FILES['image']['tmp_name'];
    	$image_type = $_FILES['image']['type'];
    	$path = "image/$image";
    
    	if($image_type=='image/png' ||$image_type=='image/jpeg')
    		move_uploaded_file($image_tmp, $path);
    
    	if(empty($fname))
    	{
    		$fnerror  = "* Required !";
    	}
    	else 
    	{
    		if (!preg_match("/^[a-zA-Z ]*$/",$fname)) 
    		{
    			$fnerror = "Only letters and white space allowed";
    		}
    	}
    	if(empty($lname))
    	{
    		$lnerror="* Required !";
    	}
    	else 
    	{
    		if (!preg_match("/^[a-zA-Z ]*$/",$lname)) 
    		{
    			$lnerror = "Only letters and white space allowed";
    		}
    	}
    	if(empty($email))
    	{
    		$emerror="* Required !";
    	}
    	if(empty($pass))
    	{
    		$perror="* Required !";
    	}
    	if(empty($gender))
    	{
    		$gnerror="* Required !";
    	}
    	if(empty($num))
    	{
    		$nerror="* Required !";
    	}
    	if(empty($country))
    	{
    		$cerror="* Required !";
    	}
    	if(empty($hobby))
    	{
    		$herror="* Required !";
    	}
    	if(empty($image))
    	{
    		$ierror="* Required !";
    	}
    
    	else
    	{
    		$set = "INSERT INTO `verify`(`fname`,`lname`,`email`,`password`,`Gender`,`number`,`country`,`hobbies`,`image`,`date`,`time`) VALUES ('$fname','$lname','$email','$pass','$gender','$num','$country','$hobby','$image','$date','$time')";
    		$set_query = mysqli_query($conn,$set);
    		if ($set_query == TRUE)
    		{
    			echo "<script type='text/javascript'>
    			alert('Data Inserted Sucessfully');
    			window.location = 'display.php'; 
    			</script>";
    		}
    
    		else
    		{
    			echo "Unable To Insert Data";
    		}
    	}
    }
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Registor Form</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="bootstrap.css">
    </head>
    <style>
        .container
        {
        margin-top: 50px;
        }
    </style>
    <body>
        <div class="container">
            <form method="post" enctype="multipart/form-data" style="background-color: lightgray" class="form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <center>REGISTRATION FORM</center>
                    </h3>
                    <br>
                </div>
                <label class="control-label col-sm-2" for="fname">First name:-<i class="glyphicon glyphicon-user" style="font-size:15px;color:green"></i></label>
                <div class="col-sm-10"><input type="text" name="fname" class="form-control" placeholder="First Name" value="<?php if(isset($fname)){echo $fname;}?>">
                    <span style="color: red"><?php echo $fnerror; ?></span>
                </div>
                <br><br>
                <label class="control-label col-sm-2" for="lname">Last name:-</label>
                <div class="col-sm-10"><input type="text" class="form-control" placeholder="Last Name" name="lname" value="<?php if(isset($lname)){echo $lname;}?>" title="Name Must Be a Charectors A-Z">
                    <span style="color: red"><?php echo $lnerror; ?></span>
                </div>
                <br><br>
                <label class="control-label col-sm-2" for="email">Email:-</label>
                <div class="col-sm-10"><input type="email" class="form-control" placeholder="Email" name="email" value="<?php if(isset($email)){echo $email;}?>" pattern="(?=.*\d)(?=.*[a-z0-9]+\@([a-z])+\.([a-z]))+$" title="Email ex:- Nitesh25@gmail.com">
                    <span style="color: red"><?php echo $emerror; ?></span>
                </div>
                <br><br>
                <label class="control-label col-sm-2" for="password">Password:-</label>
                <div class="col-sm-10" > <input type="password" class="form-control" placeholder="Password" name="password" value="<?php if(isset($pass)){echo $pass;}?>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}" title="Password Must Be 8 Charectors Example@123">
                    <span style="color: red"><?php echo $perror; ?></span>
                </div>
                <br><br>
                <label class="control-label col-sm-2" for="Gender">Gender:-</label>
                <div class="col-sm-3"><label><input type="radio" name="Gender" value="Male">Male</label></div>
                <div class="col-sm-3"><label><input type="radio" name="Gender" value="Female">Female</label></div>
                <div class="col-sm-3"><label><input type="radio" name="Gender" value="Other">Other</label>
                    <span style="color: red"><?php echo $gnerror; ?></span>
                </div>
                <br><br>
                <label class="control-label col-sm-2" for="number">Mobile No:-</label>
                <div class="col-sm-10"><input type="num" class="form-control" placeholder="Number" name="number" value="<?php if(isset($num)){echo $num;}?>" pattern="(?=,*\d)(?=,*[0-9]).{10,}" title="Number Must Be 10 Digits"> 
                    <span style="color: red"><?php echo $nerror; ?></span>
                </div>
                <br><br>
                <label class="control-label col-sm-2" for="country">Country:-</label>
                <div class="col-sm-10">
                    <input list="country" class="form-control" placeholder="country" name="country" value="<?php if(isset($country)){echo $country;}?>">
                    <datalist id="country">
                        <option value="">-- Select --</option>
                        <option value="India"></option>
                        <option value="London"></option>
                        <option value="Argentina"></option>
                        <option value="South Africa"></option>
                        <option value="Canada"></option>
                    </datalist>
                    <span style="color: red"><?php echo $cerror; ?></span>
                </div>
                <br><br>
                <label class="control-label col-sm-2" for="hobbies">Hobbies:-</label>
                <div class="col-sm-2"><label><input type="checkbox" name="hobbies[]" value="Sports">&nbsp;Sports</label></div>
                <div class="col-sm-2"><label> <input type="checkbox" name="hobbies[]" value="Travalling">&nbsp;Travalling</label></div>
                <div class="col-sm-2"><label> <input type="checkbox" name="hobbies[]" value="Dancing">&nbsp;Dancing</label></div>
                <div class="col-sm-2"><label> <input type="checkbox" name="hobbies[]" value="Singing">&nbsp;Singing</label></div>
                <span style="color: red"><?php echo $herror; ?></span>
                <br><br>
                <label class="control-label col-sm-2" for="image">File Upload:-</label>
                <div class="col-sm-10"><input type="file"   name="image" value="<?php if(isset($image)){echo $image;}?>">
                    <span style="color: red"><?php echo $ierror; ?></span>
                </div>
                <br><br>
                <div class="form-group" >
                    <div class="col-lg-offset-4 col-md-4">
                        <input type="submit" class="btn btn-md btn-success btn-block" value="Submit" name="submit">
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>