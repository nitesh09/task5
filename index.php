<?php
	session_start();
	include("config.php");
	error_reporting(0);
	if(isset($_SESSION['email']))
		{
			header("location:logout.php");
		}
		date_default_timezone_set("Asia/Kolkata");
		?>
<?php
	if(isset($_POST['submit']))
	{
		header ("location:insert.php");
	}
	
	if(isset($_POST['login']))
	{
		$email = $_POST['email'];
		$pass = $_POST['password'];
	
		$login = mysqli_query($conn, " SELECT * from verify where email = '".$email."' and password = '".$pass."' ");
		$no = mysqli_num_rows($login);
	
		if($no == 1)
		{
			$login_data = mysqli_fetch_array($login);
				$_SESSION['user_id'] = $login_data['id']; //Data Declare!
				$_SESSION['fname'] = $login_data['fname'];
				$_SESSION['lname'] = $login_data['lname'];
				$_SESSION['email'] = $login_data['email'];
				$_SESSION['password'] = $login_data['password'];
				$_SESSION['Gender'] = $login_data['Gender'];
				$_SESSION['number'] = $login_data['number'];
				$_SESSION['country'] = $login_data['country'];
				$_SESSION['hobbies'] = $login_data['hobbies'];
				$_SESSION['image'] = $login_data['image'];
			// $_SESSION['date'] = $login_data['date'];
			// $_SESSION['time'] = $login_data['time'];  Date and Time Not Defined
				header("location:logout.php");
				}
				else {
					header("location:index.php");
				}
		}
		?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Login</title>
		<link rel="stylesheet" href="bootstrap.css">
	</head>
	<style>
		.login-panel {
		margin-top: 150px;
		}
	</style>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="login-panel panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">Sign In</h3>
						</div>
						<div class="panel-body">
							<form role="form" method="post">
								<fieldset>
									<div class="form-group">
										<input class="form-control" placeholder="E-mail" name="email" type="email" autofocus pattern="(?=.*\d)(?=.*[a-z0-9]+\@([a-z])+\.([a-z]))+$" title="Email ex:- Nitesh25@gmail.com">
									</div>
									<div class="form-group">
										<input class="form-control" placeholder="Password" name="password" type="password" value="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}" title="Password Must Be 8 Charectors Example@123">
									</div>
									<div class="checkbox">
										<label><input type="checkbox">Remember me</label>
									</div>
									<input class="btn btn-lg btn-success btn-block" type="submit" value="login" name="login" >
									<button class="btn btn-lg btn-basic btn-block"><a href="insert.php">Registration</a></button><br>
									<button class="btn btn-sm btn-basic"><a href="newpass.php">Forget Password?</a></button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>